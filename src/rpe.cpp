
#define PROGRAM_NAME \
    "rpe"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(double, time_offset, 0.0, "Time offset added to input trajectory")                   \
    FLAG_CASE(bool, compute_scale, false, "Compute scaling factor?")                               \
    FLAG_CASE(double, scale, 1.0, "Scale factor for input trajectory")                             \
    FLAG_CASE(bool, per_second, false, "Compute the RPE per second metric")                        \
    FLAG_CASE(uint64, o, 0, "Sequence offset for input trajectory")                                \
    FLAG_CASE(uint64, s, 0, "Skip sequence elements for input trajectory")                         \
    FLAG_CASE(uint64, n, 0, "Max sequence length for input trajectory")                            \
    FLAG_CASE(string, output_file, "", "Output error file")

#define ARGS_CASES                                                                                 \
    ARG_CASE(reference)                                                                            \
    ARG_CASE(trajectory)

// STL
#include <algorithm>
#include <cmath>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <iterator>
#include <limits>
#include <vector>

// Boost
#include <boost/filesystem/operations.hpp>

// Eigen
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "args.hpp"
#include "associate.hpp"
#include "decimate.hpp"
#include "io.hpp"
#include "macros.h"
#include "maths.hpp"
#include "statistics.hpp"
#include "version.h"

inline double translation_distance(const Eigen::Isometry3d& T) {
    return T.translation().norm();
}

inline double rotation_angle(const Eigen::Isometry3d& T) {
    return std::acos(Clip(0.5*(T.linear().trace() - 1.0), -1.0, 1.0));
}

void ValidateArgs() {
    RUNTIME_ASSERT(boost::filesystem::is_regular_file(ARGS_reference));
    RUNTIME_ASSERT(boost::filesystem::is_regular_file(ARGS_trajectory));
}

void ValidateFlags() {
    RUNTIME_ASSERT(FLAGS_scale > 0.0);
}

int main(int argc, char *argv[]) {

    // Print build info
    std::cout << PROGRAM_NAME << " (" << GetBuildInfo() << ")" << std::endl;
    std::cout << std::endl;

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateFlags();
    ValidateArgs();

    // Read input trajectories
    io::Trajectory reference = io::read_file<io::Trajectory::value_type>(ARGS_reference);
    RUNTIME_ASSERT(!reference.empty());

    io::Trajectory trajectory = io::read_file<io::Trajectory::value_type>(ARGS_trajectory);
    RUNTIME_ASSERT(!trajectory.empty());

    // Decimate input trajectory
    trajectory = decimate_sequence(trajectory, FLAGS_o, FLAGS_s, FLAGS_n);
    RUNTIME_ASSERT(trajectory.size() >= 2);

    if (std::abs(FLAGS_time_offset) > 0.0) {
        for (io::Trajectory::value_type& elem : trajectory)
            elem.timestamp += FLAGS_time_offset;
    }

    std::vector<io::timestamp_t> ref_intervals(reference.size());
    for (std::size_t i = 1; i < reference.size(); ++i)
        ref_intervals.at(i) = reference.at(i).timestamp - reference.at(i-1).timestamp;

    io::timestamp_t ref_max_time = 0.5*vector_median<io::timestamp_t>(ref_intervals);

    std::vector<double> scales;
    using index_pair = std::pair<std::size_t, std::size_t>;
    std::vector<std::pair<index_pair, index_pair>> pairs;
    pairs.reserve(trajectory.size());
    for (io::Trajectory::const_iterator traj_cit_prev = trajectory.begin(); traj_cit_prev != trajectory.end(); ++traj_cit_prev) {        
        io::Trajectory::const_iterator traj_cit_next = std::next(traj_cit_prev);
        if (traj_cit_next == trajectory.end()) continue;

        io::Trajectory::const_iterator ref_cit_prev = find_closest(reference, traj_cit_prev->timestamp);
        io::Trajectory::const_iterator ref_cit_next = find_closest(reference, traj_cit_next->timestamp);

        if (ref_cit_prev == reference.end() || ref_cit_next == reference.end())
            continue;

        if (abs_diff(ref_cit_prev->timestamp, traj_cit_prev->timestamp) > ref_max_time ||
            abs_diff(ref_cit_next->timestamp, traj_cit_next->timestamp) > ref_max_time)
            continue;

        if (FLAGS_compute_scale) {
            Eigen::Vector3d traj_t0(traj_cit_prev->pose.tx, traj_cit_prev->pose.ty, traj_cit_prev->pose.tz);
            Eigen::Vector3d traj_t1(traj_cit_next->pose.tx, traj_cit_next->pose.ty, traj_cit_next->pose.tz);

            Eigen::Vector3d ref_t0(ref_cit_prev->pose.tx, ref_cit_prev->pose.ty, ref_cit_prev->pose.tz);
            Eigen::Vector3d ref_t1(ref_cit_next->pose.tx, ref_cit_next->pose.ty, ref_cit_next->pose.tz);

            scales.emplace_back((ref_t1 - ref_t0).norm() / (traj_t1 - traj_t0).norm());
        }

        pairs.emplace_back(std::make_pair(
                             std::make_pair(std::distance(reference.cbegin(), ref_cit_prev), std::distance(trajectory.cbegin(), traj_cit_prev)),
                             std::make_pair(std::distance(reference.cbegin(), ref_cit_next), std::distance(trajectory.cbegin(), traj_cit_next))
                           ));
    }

    double scale_factor = FLAGS_scale;
    if (FLAGS_compute_scale) {
        RUNTIME_ASSERT(!scales.empty());
        scale_factor = vector_median(scales);
    }
    std::cout << "scale: " << scale_factor << std::endl;

    const int N = pairs.size();
    std::cout << "compared_pose_pairs " << N << " pairs" << std::endl;
    RUNTIME_ASSERT(!pairs.empty());

    std::vector<double> translation(N, std::numeric_limits<double>::quiet_NaN()), 
                        rotation(N, std::numeric_limits<double>::quiet_NaN());
    for (int i = 0; i < N; ++i) {
        const std::pair<index_pair, index_pair>& match = pairs.at(i);

        Eigen::Isometry3d dQ = Eigen::Isometry3d(reference.at(match.first.first).pose).inverse() * Eigen::Isometry3d(reference.at(match.second.first).pose);
        Eigen::Isometry3d dP = Eigen::Isometry3d(trajectory.at(match.first.second).pose).inverse() * Eigen::Isometry3d(trajectory.at(match.second.second).pose);
        dP.translation() *= scale_factor; //TODO proove!

        double factor = 1.0;
        if (FLAGS_per_second) {
            factor /= trajectory.at(match.second.second).timestamp - trajectory.at(match.first.second).timestamp;
        }

        Eigen::Isometry3d Ei = dQ.inverse() * dP;
        translation.at(i) = translation_distance(Ei) * factor;
        rotation.at(i) = rotation_angle(Ei) * factor;
    }

    // Translation
    std::cout << "translational_error.rmse " << vector_rms<double>(translation) << " m" << std::endl;
    std::cout << "translational_error.mean " << vector_mean<double>(translation) << " m" << std::endl;
    std::cout << "translational_error.median " << vector_median<double>(translation) << " m" << std::endl;
    std::cout << "translational_error.std " << vector_stdv<double>(translation) << " m" << std::endl;

    std::pair<std::vector<double>::const_iterator, std::vector<double>::const_iterator> min_max_trans
        = std::minmax_element(translation.cbegin(), translation.cend());
    std::cout << "translational_error.min " << *(min_max_trans.first) << " m" << std::endl;
    std::cout << "translational_error.max " << *(min_max_trans.second) << " m" << std::endl;

    // Rotation
    std::cout << "rotational_error.rmse " << RadToDeg(vector_rms<double>(rotation)) << " deg" << std::endl;
    std::cout << "rotational_error.mean " << RadToDeg(vector_mean<double>(rotation)) << " deg" << std::endl;
    std::cout << "rotational_error.median " << RadToDeg(vector_median<double>(rotation)) << " deg" << std::endl;
    std::cout << "rotational_error.std " << RadToDeg(vector_stdv<double>(rotation)) << " deg" << std::endl;

    std::pair<std::vector<double>::const_iterator, std::vector<double>::const_iterator> min_max_rot
        = std::minmax_element(rotation.cbegin(), rotation.cend());
    std::cout << "rotational_error.min " << RadToDeg(*(min_max_rot.first)) << " deg" << std::endl;
    std::cout << "rotational_error.max " << RadToDeg(*(min_max_rot.second)) << " deg" << std::endl;

    if (!FLAGS_output_file.empty()) {
        std::ofstream output_file(FLAGS_output_file);
        for (int i = 0; i < N; ++i)
            output_file << translation.at(i) << " " << rotation.at(i) << std::endl;
    }

    return 0;
}
