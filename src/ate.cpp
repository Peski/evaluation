
#define PROGRAM_NAME \
    "ate"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(double, time_offset, 0.0, "Time offset added to trajectory")                         \
    FLAG_CASE(double, max_difference, 0.01, "Max time difference allowed")                         \
    FLAG_CASE(bool, compute_scale, false, "Compute scale factor?")                                 \
    FLAG_CASE(double, scale, 1.0, "Scale factor for input trajectory")                             \
    FLAG_CASE(uint64, o, 0, "Sequence offset for input trajectory")                                \
    FLAG_CASE(uint64, s, 0, "Skip sequence elements for input trajectory")                         \
    FLAG_CASE(uint64, n, 0, "Max sequence length for input trajectory")                            \
    FLAG_CASE(string, output_errors, "", "Output errors file")                                     \
    FLAG_CASE(bool, T, false, "Output the 4x4 transformation matrix")                              \
    FLAG_CASE(string, output_pairs, "", "Output the associated timestamps")

#define ARGS_CASES                                                                                 \
    ARG_CASE(reference)                                                                            \
    ARG_CASE(trajectory)

// STL
#include <algorithm>
#include <cmath>
#include <cstddef>
#include <fstream>
#include <iomanip>
#include <ios>
#include <iostream>
#include <vector>

// Boost
#include <boost/filesystem/operations.hpp>

// Eigen
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "args.hpp"
#include "associate.hpp"
#include "decimate.hpp"
#include "io.hpp"
#include "macros.h"
#include "statistics.hpp"
#include "version.h"

namespace io {

using pair_t = std::pair<io::timestamp_t, io::timestamp_t>;

inline std::ostream& operator<<(std::ostream& lhs, const pair_t& rhs) {

    lhs << std::to_string(rhs.first) << OUTPUT_SEPARATOR << std::to_string(rhs.second);
    return lhs;
}

}

void ValidateArgs() {
    RUNTIME_ASSERT(boost::filesystem::is_regular_file(ARGS_reference));
    RUNTIME_ASSERT(boost::filesystem::is_regular_file(ARGS_trajectory));
}

void ValidateFlags() {
    RUNTIME_ASSERT(FLAGS_max_difference > 0.0);
    RUNTIME_ASSERT(FLAGS_scale > 0.0);
}

int main(int argc, char *argv[]) {

    io::OUTPUT_SEPARATOR = " ";

    // Print build info
    std::cout << PROGRAM_NAME << " (" << GetBuildInfo() << ")" << std::endl;
    std::cout << std::endl;

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateFlags();
    ValidateArgs();    

    // Read input trajectories
    io::Trajectory reference = io::read_file<io::Trajectory::value_type>(ARGS_reference);
    io::Trajectory trajectory = io::read_file<io::Trajectory::value_type>(ARGS_trajectory);

    if (std::abs(FLAGS_time_offset) > 0.0) {
        for (io::Trajectory::value_type& elem : trajectory)
            elem.timestamp += FLAGS_time_offset;
    }

    // Decimate reference trajectory
    reference = decimate_sequence(reference, FLAGS_o, FLAGS_s, FLAGS_n);

    // Compute pose associations
    std::vector<std::pair<std::size_t, std::size_t>> pairs = associate(reference, trajectory, FLAGS_max_difference);
    const int N = pairs.size();
    RUNTIME_ASSERT(N >= 3);

    // Compute least-squares rigid body transformation
    Eigen::MatrixXd src(3, N);
    Eigen::MatrixXd dst(3, N);

    int index = 0;
    for (const std::pair<std::size_t, std::size_t>& match : pairs) {
        const io::pose_t ref = reference.at(match.first).pose;
        const io::pose_t traj = trajectory.at(match.second).pose;

        dst.col(index) = Eigen::Vector3d(ref.tx, ref.ty, ref.tz);
        src.col(index) = Eigen::Vector3d(traj.tx, traj.ty, traj.tz);
        index++;
    }

    Eigen::Matrix4d Tmatrix = Eigen::umeyama(src, dst, FLAGS_compute_scale);
    Eigen::Isometry3d S = Eigen::Isometry3d::Identity();
    S.linear() = Tmatrix.block<3, 3>(0, 0);
    S.translation() = Tmatrix.block<3, 1>(0, 3);

    double scale_factor = FLAGS_scale;
    if (FLAGS_compute_scale) {
        scale_factor = std::cbrt(S.linear().determinant());
        S.linear() /= scale_factor;
    }

    std::cout << std::fixed;
    std::cout << std::setprecision(4);
    std::cout << "scale: " << scale_factor << std::endl;
    std::cout << "compared_pose_pairs " << N << " pairs" << std::endl;

    // Compute absolute value errors
    std::vector<double> abs_errors(N);
    for (int i = 0; i < N; ++i) {
        std::pair<std::size_t, std::size_t> match = pairs.at(i);

        Eigen::Isometry3d Qi = reference.at(match.first).pose;
        Eigen::Isometry3d Pi = trajectory.at(match.second).pose;
        Pi.translation() *= scale_factor;

        Eigen::Isometry3d Fi = Qi.inverse()*S*Pi;
        abs_errors.at(i) = Fi.translation().norm();
    }

    std::cout << "absolute_translational_error.rmse " << vector_rms<double>(abs_errors) << " m" << std::endl;
    std::cout << "absolute_translational_error.mean " << vector_mean<double>(abs_errors) << " m" << std::endl;
    std::cout << "absolute_translational_error.median " << vector_median<double>(abs_errors) << " m" << std::endl;
    std::cout << "absolute_translational_error.std " << vector_stdv<double>(abs_errors) << " m" << std::endl;

    std::pair<std::vector<double>::const_iterator, std::vector<double>::const_iterator> min_max 
         = std::minmax_element(abs_errors.cbegin(), abs_errors.cend());
    std::cout << "absolute_translational_error.min " << *(min_max.first) << " m" << std::endl;
    std::cout << "absolute_translational_error.max " << *(min_max.second) << " m" << std::endl;

    if (FLAGS_T) {
        std::cout << std::fixed;
        std::cout << std::setprecision(12);
        std::cout << std::endl << "T: " << std::endl;
        S.linear() *= scale_factor;
        std::cout << S.matrix() << std::endl;
    }

    if (!FLAGS_output_pairs.empty()) {
        std::vector<io::pair_t> associations;
        for (const std::pair<std::size_t, std::size_t>& match : pairs) {
            const io::timestamp_t ref_ts = reference.at(match.first).timestamp;
            const io::timestamp_t traj_ts = trajectory.at(match.second).timestamp;

            if (std::abs(FLAGS_time_offset) > 0.0)
                associations.push_back(std::make_pair(ref_ts, traj_ts - FLAGS_time_offset));
            else
                associations.push_back(std::make_pair(ref_ts, traj_ts));            
        }

        io::write_file<io::pair_t>(associations, FLAGS_output_pairs);
    }

    if (!FLAGS_output_errors.empty()) {
        std::ofstream output_file(FLAGS_output_errors);
        for (int i = 0; i < N; ++i)
            output_file << abs_errors.at(i) << std::endl;
    }

    return 0;
}

