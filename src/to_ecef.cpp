
#define PROGRAM_NAME \
    "to_ecef"

// TODO No all options are implemented yet
#define FLAGS_CASES                                                                                \
    FLAG_CASE(int64, time_offset, 0, "Time offset added to reference [ns]")                        \
    FLAG_CASE(double, max_difference, 0.01, "Max time difference allowed")                         \
    FLAG_CASE(bool, compute_scale, false, "Compute scale factor?")                                 \
    FLAG_CASE(double, scale, 1.0, "Scale factor for input trajectory")                             \
    FLAG_CASE(uint64, o, 0, "Sequence offset for input trajectory")                                \
    FLAG_CASE(uint64, s, 0, "Skip sequence elements for input trajectory")                         \
    FLAG_CASE(uint64, n, 0, "Max sequence length for input trajectory")                            \
    FLAG_CASE(string, output_errors, "", "Output errors file")                                     \
    FLAG_CASE(bool, T, false, "Output the 4x4 transformation matrix")                              \
    FLAG_CASE(string, output_pairs, "", "Output the associated timestamps")

#define ARGS_CASES                                                                                 \
    ARG_CASE(reference)                                                                            \
    ARG_CASE(trajectory)                                                                           \
    ARG_CASE(output)

// STL
#include <algorithm>
#include <cmath>
#include <cstddef>
#include <fstream>
#include <iomanip>
#include <ios>
#include <iostream>
#include <vector>

// Boost
#include <boost/filesystem/operations.hpp>

// Eigen
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "args.hpp"
#include "associate.hpp"
#include "csv.hpp"
#include "decimate.hpp"
#include "gps.hpp"
#include "io.hpp"
#include "macros.h"
#include "statistics.hpp"
#include "version.h"

#include <type_traits>

namespace io {

using pair_t = std::pair<io::timestamp_t, io::timestamp_t>;

inline std::ostream& operator<<(std::ostream& lhs, const pair_t& rhs) {

    lhs << std::to_string(rhs.first) << OUTPUT_SEPARATOR << std::to_string(rhs.second);
    return lhs;
}

}

void ValidateArgs() {
    RUNTIME_ASSERT(boost::filesystem::is_regular_file(ARGS_reference));
    RUNTIME_ASSERT(boost::filesystem::is_regular_file(ARGS_trajectory));
}

void ValidateFlags() {
    RUNTIME_ASSERT(FLAGS_max_difference > 0.0);
    RUNTIME_ASSERT(FLAGS_scale > 0.0);
}

int main(int argc, char *argv[]) {

    static_assert(std::is_same<io::timestamp_t, double>::value, "io::timestamp_t must be double");

    io::OUTPUT_SEPARATOR = " ";
    io::OUTPUT_PRECISION = 7;

    // Print build info
    std::cout << PROGRAM_NAME << " (" << GetBuildInfo() << ")" << std::endl;
    std::cout << std::endl;

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateFlags();
    ValidateArgs();    

    // Read input trajectories
    //io::GPSTrajectory reference = io::read_file<io::GPSTrajectory::value_type>(ARGS_reference);    
    io::GPSTrajectory reference;
    Eigen::MatrixXd data = csv::read<double>(ARGS_reference, ' ');
    RUNTIME_ASSERT(data.cols() == 8); // 8 columns expected
    RUNTIME_ASSERT(data.rows() > 0); // invalid file?
    for (int i = 0; i < data.rows(); ++i) {
        io::gps_t gps_data(data(i, 1), data(i, 2), data(i, 3),
                           data(i, 5), data(i, 6), data(i, 7), data(i, 4));
        
        reference.emplace_back(data(i, 0) + FLAGS_time_offset * 1e-9, gps_data);
    }
    
    io::Trajectory trajectory = io::read_file<io::Trajectory::value_type>(ARGS_trajectory);

    // Decimate input trajectory
    trajectory = decimate_sequence(trajectory, FLAGS_o, FLAGS_s, FLAGS_n);

    // Compute pose associations
    std::vector<std::pair<std::size_t, std::size_t>> pairs = associate(reference, trajectory, FLAGS_max_difference);
    const int N = pairs.size();
    RUNTIME_ASSERT(N >= 3);

    // Make sure we star from the first trajectory pose (shouldn't be needed, but just in case...)
    std::sort(pairs.begin(), pairs.end(), [](const std::pair<std::size_t, std::size_t>& lhs, const std::pair<std::size_t, std::size_t>& rhs){
        return lhs.second < rhs.second;
      });
    
    io::gps_t ref_0 = reference.at(pairs.front().first).pose;
    io::pose_t traj_0 = trajectory.at(pairs.front().second).pose;
    
    io::gps_t ref_1 = reference.at(pairs.at(1).first).pose;
    io::pose_t traj_1 = trajectory.at(pairs.at(1).second).pose;
    
    Eigen::Vector3d ecef_0 = gps::geodetic2Ecef(ref_0.phi, ref_0.lambda, ref_0.h);
    Eigen::Vector3d ecef_1 = gps::geodetic2Ecef(ref_1.phi, ref_1.lambda, ref_1.h);
    
    double scale = (ecef_1 - ecef_0).norm() / (traj_1.translation() - traj_0.translation()).norm();
    
    // ECEF to NED_0 tranformation
    Eigen::Matrix3d R_ne = gps::nRe(ref_0.phi, ref_0.lambda);
    Eigen::Isometry3d T_ne = Eigen::Isometry3d::Identity();
    T_ne.linear() = R_ne;
    T_ne.translation() = - R_ne * ecef_0;
    
    // Body_0 to NED_0
    Eigen::Isometry3d T_nb = Eigen::Isometry3d::Identity();
    T_nb.linear() = ref_0.rotation();
    
    // Initial trajectory pose
    Eigen::Isometry3d T_0 = traj_0;
    
    // Camera to body transformation
    Eigen::Matrix3d R_bc;
    R_bc <<  0.0, -1.0,  0.0,
             1.0,  0.0,  0.0,
             0.0,  0.0,  1.0;
    Eigen::Isometry3d T_bc = Eigen::Isometry3d::Identity();
    T_bc.linear() = R_bc;
    
    io::Trajectory ecef_tum;
    for (const std::pair<std::size_t, std::size_t>& entry : pairs) {
        std::size_t index = entry.second;
        io::pose_t traj = trajectory.at(index).pose;
        
        Eigen::Isometry3d T_rel =  T_0.inverse() * traj;
        T_rel.translation() *= scale;
        
        Eigen::Isometry3d T_e = T_ne.inverse() * T_nb * T_bc * T_rel;
        ecef_tum.emplace_back(trajectory.at(index).id, T_e);
    }
    
    io::write_file<io::Trajectory::value_type>(ecef_tum, ARGS_output, "#timestamp [s] ecef_x [m] ecef_y [m] ecef_z [m] qx qy qz qw");

    return 0;
}
