
#define PROGRAM_NAME \
    "ate"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(double, time_offset, 0.0, "Time offset added to trajectory")                         \
    FLAG_CASE(double, scale, 1.0, "Scale factor for trajectory")                                   \
    FLAG_CASE(uint64, o, 0, "Sequence offset for input trajectory")                                \
    FLAG_CASE(uint64, s, 0, "Skip sequence elements for input trajectory")                         \
    FLAG_CASE(uint64, n, 0, "Max sequence length for input trajectory")

#define ARGS_CASES                                                                                 \
    ARG_CASE(trajectory)

// STL
#include <algorithm>
#include <cmath>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <vector>

// Boost
#include <boost/filesystem/operations.hpp>

// Eigen
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "args.hpp"
#include "associate.hpp"
#include "decimate.hpp"
#include "io.hpp"
#include "macros.h"
#include "statistics.hpp"
#include "version.h"

void ValidateArgs() {
    RUNTIME_ASSERT(boost::filesystem::is_regular_file(ARGS_trajectory));
}

void ValidateFlags() {
    RUNTIME_ASSERT(FLAGS_scale > 0.0);
}

int main(int argc, char *argv[]) {

    io::OUTPUT_SEPARATOR = " ";

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateFlags();
    ValidateArgs();

    // Read input trajectories
    io::Trajectory trajectory = io::read_file<io::Trajectory::value_type>(ARGS_trajectory);

    // Decimate reference trajectory
    trajectory = decimate_sequence(trajectory, FLAGS_o, FLAGS_s, FLAGS_n);

    // Transform
    for (io::Trajectory::value_type& elem : trajectory) {
        elem.timestamp += FLAGS_time_offset;

        elem.pose.tx *= FLAGS_scale;
        elem.pose.ty *= FLAGS_scale;
        elem.pose.tz *= FLAGS_scale;
    }

    // Output
    io::write_stream<io::Trajectory::value_type>(trajectory, std::cout);

    return 0;
}
