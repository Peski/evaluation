
#define PROGRAM_NAME \
    "io_test"

#define FLAGS_CASES                                                                                \

#define ARGS_CASES                                                                                 \

// STL
#include <algorithm>
#include <cmath>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <vector>

// Boost
#include <boost/filesystem/operations.hpp>

// Eigen
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "args.hpp"
#include "associate.hpp"
#include "decimate.hpp"
#include "io.hpp"
#include "macros.h"
#include "statistics.hpp"
#include "version.h"

#include <type_traits>

namespace io {

using pair_t = std::pair<io::timestamp_t, io::timestamp_t>;

inline std::ostream& operator<<(std::ostream& lhs, const pair_t& rhs) {

    lhs << std::to_string(rhs.first) << OUTPUT_SEPARATOR << std::to_string(rhs.second);
    return lhs;
}

inline std::istream& operator>>(std::istream& lhs, pair_t& rhs) {

    lhs >> rhs.first >> rhs.second;
    return lhs;
}

}

void ValidateArgs() {
}

void ValidateFlags() {
}

int main(int argc, char *argv[]) {

    io::timestamp_t dummy_t;
    static_assert(std::is_same<decltype(dummy_t), double>::value, "io::timestamp_t must be double");

    io::OUTPUT_SEPARATOR = " ";

    // Print build info
    std::cout << PROGRAM_NAME << " (" << GetBuildInfo() << ")" << std::endl;
    std::cout << std::endl;

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateFlags();
    ValidateArgs();

    // Read input trajectories
    io::Trajectory input = io::read_file<io::Trajectory::value_type>("./test/read_data.txt");
    RUNTIME_ASSERT(!input.empty());
    RUNTIME_ASSERT(input.size() == 1);

    io::Trajectory::value_type elem = input.front();
    RUNTIME_ASSERT(static_cast<unsigned long>(elem.timestamp * 10.0) == 314159265359ul);
    RUNTIME_ASSERT(static_cast<unsigned>(elem.pose.tx) == 0u);
    RUNTIME_ASSERT(static_cast<unsigned>(elem.pose.ty) == 0u);
    RUNTIME_ASSERT(static_cast<unsigned>(elem.pose.tz) == 0u);
    RUNTIME_ASSERT(static_cast<unsigned>(elem.pose.qx) == 0u);
    RUNTIME_ASSERT(static_cast<unsigned>(elem.pose.qy) == 0u);
    RUNTIME_ASSERT(static_cast<unsigned>(elem.pose.qz) == 0u);
    RUNTIME_ASSERT(static_cast<unsigned>(elem.pose.qw) == 1u);

    std::cout << "Read test OK" << std::endl;

    std::vector<io::pair_t> output = {{1337.0, 57005.0}};
    io::write_file<io::pair_t>(output, "./test/write_data.txt");

    output.clear();

    std::ifstream out_in("./test/write_data.txt");
    RUNTIME_ASSERT(out_in.is_open());

    double dummy1, dummy2;
    out_in >> dummy1 >> dummy2;

    RUNTIME_ASSERT(static_cast<unsigned>(dummy1) == 1337u);
    RUNTIME_ASSERT(static_cast<unsigned>(dummy2) == 0xDEADu);

    std::cout << "Write test OK" << std::endl;

    return 0;
}
