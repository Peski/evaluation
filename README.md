# Evaluation Tools
Flexible C++ evaluation tools for Visual Odometry and SLAM

**Author:** [David Zuñiga-Noël](http://mapir.isa.uma.es/mapirwebsite/index.php/people/270)

**License:**  [GPLv3](LICENSE.txt)

## Dependencies (tested)

* Boost (1.58.0.1ubuntu1):
   ```
   sudo apt install libboost-all-dev
   ```
* CMake (3.5.1-1ubuntu1):
   ```
   sudo apt install cmake
   ```
* Eigen (3.3~beta1-2):
   ```
   sudo apt install libeigen3-dev
   ```
* Gflags (2.1.2-3):
   ```
   sudo apt install libgflags-dev
   ```
## Build
Make sure all dependencies are correctly installed. To build, just run the provided `build.sh` script:
```
git clone https://bitbucket.org/Peski/evaluation.git
bash build.sh -j8
```
which should build the executables in the `./build` directory.

## Data Format
The input for evaluation are the reference trajectory (groundtruth) and the estimated trajectory you wish to evaluate.
Trajectories are representend in the TUM plain text format:
```
timestamp tx ty tz qx qy qz qw
```
with timestamps expressed in seconds.

## Usage
Two different evaluation metrics are supported: the Absoulte Trajectory Error (ATE) and the Relative Trajectory Error (RPE) [\[1\]][1].
```
./build/ate [options] <reference> <trajectory>
```

or

```
./build/rpe [options] <reference> <trajectory>
```

Use `--help` for additional information about the posible options.

\[1\] J. Sturm and N. Engelhard and F. Endres and W. Burgard and D. Cremers, "A Benchmark for the Evaluation of RGB-D SLAM Systems", IROS 2012

[1]: https://vision.in.tum.de/_media/spezial/bib/sturm12iros.pdf
