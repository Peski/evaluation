
#ifndef GPS_HPP_
#define GPS_HPP_

#include <cmath>
#include <cstdint>
#include <fstream>
#include <istream>
#include <string>
#include <utility>
#include <vector>

#include <Eigen/Core>

namespace gps {

struct GPSWithOrientation {
    double phi, lambda, h;
    double qw, qx, qy, qz;
    
    GPSWithOrientation()
        : phi(0.0), lambda(0.0), h(0.0), qw(1.0), qx(0.0), qy(0.0), qz(0.0)
    { }

    GPSWithOrientation(double phi, double lambda, double h, double qw, double qx, double qy, double qz)
        : phi(phi), lambda(lambda), h(h), qw(qw), qx(qx), qy(qy), qz(qz)
    {
        q_normalize();
    }

    inline void q_normalize() {
        double norm = std::sqrt(qw*qw + qx*qx + qy*qy + qz*qz);
        qw /= norm;
        qx /= norm;
        qy /= norm;
        qz /= norm;
    }
};

std::vector<std::pair<std::uint64_t, GPSWithOrientation>> read_gps_file(const std::string &path, char delim = ',') {
    std::vector<std::pair<std::uint64_t, GPSWithOrientation>> pairs;

    std::string line;
    std::ifstream input(path);
    while (std::getline(input, line)) {
        if (line.empty()) continue;
        if (line.front() == '#') continue;

        std::uint64_t timestamp;
        std::string str_ts, str_phi, str_lambda, str_h, str_qw, str_qx, str_qy, str_qz;
        std::stringstream line_stream(line);
        if (std::getline(line_stream, str_ts, delim)
              && std::getline(line_stream, str_phi, delim)
              && std::getline(line_stream, str_lambda, delim)
              && std::getline(line_stream, str_h, delim)
              && std::getline(line_stream, str_qw, delim)
              && std::getline(line_stream, str_qx, delim)
              && std::getline(line_stream, str_qy, delim)
              && std::getline(line_stream, str_qz, delim)) {
            timestamp = static_cast<std::uint64_t>(std::stoll(str_ts));
            GPSWithOrientation data(std::stod(str_phi), std::stod(str_lambda), std::stod(str_h),
                                    std::stod(str_qw), std::stod(str_qx), std::stod(str_qy), std::stod(str_qz));
            pairs.push_back(std::make_pair(timestamp, data));
        }
    }

    return pairs;
}

inline Eigen::Vector3d geodetic2Ecef(const double latitude, const double longitude, const double altitude) {
  // Convert geodetic coordinates to ECEF (WGS84)
  double xi = std::sqrt(1.0 - (6.69437999014 * 0.001) * std::sin(latitude) * std::sin(latitude));
  
  Eigen::Vector3d ecef;
  ecef.x() = (6378137.0 / xi + altitude) * std::cos(latitude) * std::cos(longitude);
  ecef.y() = (6378137.0 / xi + altitude) * std::cos(latitude) * std::sin(longitude);
  ecef.z() = (6378137.0 / xi * (1.0 - (6.69437999014 * 0.001)) + altitude) * std::sin(latitude);
  return ecef;
}

inline Eigen::Matrix3d nRe(const double latitude, const double longitude) {
  const double sLat = std::sin(latitude);
  const double sLon = std::sin(longitude);
  const double cLat = std::cos(latitude);
  const double cLon = std::cos(longitude);

  Eigen::Matrix3d R;
  R(0, 0) = -sLat * cLon;
  R(0, 1) = -sLat * sLon;
  R(0, 2) = cLat;
  R(1, 0) = -sLon;
  R(1, 1) = cLon;
  R(1, 2) = 0.0;
  R(2, 0) = -cLat * cLon;
  R(2, 1) = -cLat * sLon;
  R(2, 2) = -sLat;

  return R;
}

/*
void RelativeNEDCoordinates(const double latitude_0, const double longitude_0, const double altitude_0,
                            const double latitude, const double longitude, const double altitude,
                            double* x, double* y, double* z) {
    const double sLat = std::sin((latitude_0 / 180.0) * EIGEN_PI);
    const double sLon = std::sin((longitude_0 / 180.0) * EIGEN_PI);
    const double cLat = std::cos((latitude_0 / 180.0) * EIGEN_PI);
    const double cLon = std::cos((longitude_0 / 180.0) * EIGEN_PI);

    Eigen::Matrix3d nRe;
    nRe(0, 0) = -sLat * cLon;
    nRe(0, 1) = -sLat * sLon;
    nRe(0, 2) = cLat;
    nRe(1, 0) = -sLon;
    nRe(1, 1) = cLon;
    nRe(1, 2) = 0.0;
    nRe(2, 0) = -cLat * cLon;
    nRe(2, 1) = -cLat * sLon;
    nRe(2, 2) = -sLat;
    
    double ecef_x0, ecef_y0, ecef_z0;
    geodetic2Ecef(latitude_0, longitude_0, altitude_0, &ecef_x0, &ecef_y0, &ecef_z0);
    
    double ecef_x, ecef_y, ecef_z;
    geodetic2Ecef(latitude, longitude, altitude, &ecef_x, &ecef_y, &ecef_z);
    
    Eigen::Vector3d ned = nRe * Eigen::Vector3d(ecef_x - ecef_x0, ecef_y - ecef_y0, ecef_z - ecef_z0);
    
    *x = ned.x();
    *y = ned.y();
    *z = ned.z();
}
*/
} // namesapce gps

#endif // GPS_HPP_
