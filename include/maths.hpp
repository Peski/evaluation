
#ifndef MATHS_HPP_
#define MATHS_HPP_

// STL
#include <algorithm>
#include <cmath>

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288
#endif

double RadToDeg(const double rad) {
  return rad * 57.29577951308232286464772187173366546630859375;
}

template <typename T>
inline T Clip(const T& value, const T& low, const T& high) {
  return std::max(low, std::min(value, high));
}

#endif // MATHS_HPP_
